# Conference GO!

A highly functional app for creating and managing events, conferences, presentations as well as attendees for all instances. Message passing is handled by email updates via mailhog.

![Signing up for a conference and email alerts](./signupforconference.mp4)

## How to Run this App

	Repository Link: https://gitlab.com/eleet94/eventward

    Needs: Docker Desktop

	1. Fork project repository
	2. Clone the project repository to your local machine.
	3. Navigate to the project directory.
	4. Install the necessary dependencies for the front-end and back-end by running the following commands:

	- npm install
	- pip install -r requirements.txt

	then:

    docker-compose build
    docker-compose up


## Basic Functionality

    Main Page: http://localhost:3000/


![Main page with 3 demo conferences](./mainpage.mp4)
![Available forms](./formsdemo.mp4)

![Creating a new conference](./newconference.mp4)


Register to be able to sign up for presentations, events and conferences. Users are able to confirm their attendance at a conference and receive email updates on the upcoming conferences they sign up for. The public APIs utilized in this app will automatically generate weather data, location data as well as find pictures for the locations where events are held based on city name.
